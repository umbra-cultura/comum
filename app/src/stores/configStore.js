import { defineStore } from "pinia";
import { AppVisibility, Dark, LocalStorage, Notify, setCssVar } from "quasar";
import { Store } from "../utils/tauri-store";

import { ref, computed } from "vue";

export const useConfigStore = defineStore("configStore", () => {
  const store = new Store(".comum.settings.json");

  const Counter = {
    REST: "Rest",
    SHORTWORK: "ShortWork",
    LONGWORK: "LongWork",
  };

  const doropomo = ref({
    restCount: LocalStorage.getItem("restCount") || 25,
    shortWorkCount: LocalStorage.getItem("shortWorkCount") || 5,
    longWorkCount: LocalStorage.getItem("longWorkCount") || 15,
    clockCount: (LocalStorage.getItem("restCount") || 25) * 60,
    currentTimer: Counter.REST,
    isPlaying: false,
  });

  const themes = ref([
    {
      name: "Theme 1",
      primary: "#BC50F9",
      secondary: "#251F47",
      accent: "#71B48D",
    },
    {
      name: "Theme 2",
      primary: "#463F3A",
      secondary: "#BCB8B1",
      accent: "#E0AFA0",
    },
    {
      name: "Theme 3",
      primary: "#4B5842",
      secondary: "#B7CE63",
      accent: "#DADDD8",
    },
    {
      name: "Theme 4",
      primary: "#A833B9",
      secondary: "#6A5B6E",
      accent: "#88D18A",
    },
  ]);

  const currentTheme = ref(LocalStorage.getItem("theme") || "Theme 1");

  let loop = undefined;

  const isPlaying = computed(() => doropomo.value.isPlaying);
  const currentTimer = computed(() => doropomo.value.currentTimer);
  const textTimer = computed(() => {
    let minutes = Math.floor(doropomo.value.clockCount / 60);
    let seconds = doropomo.value.clockCount % 60;

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    return `${doropomo.value.currentTimer}: ${minutes}:${seconds}`;
  });

  const playPause = () => {
    if (doropomo.value.isPlaying) {
      clearInterval(loop);
      doropomo.value.isPlaying = false;
    } else {
      loop = setInterval(() => {
        if (doropomo.value.clockCount === 0) {
          if (AppVisibility.appVisible) {
            Notify.create({
              type: "info",
              message:
                doropomo.value.currentTimer === Counter.REST
                  ? "Hora do trabalho curto!"
                  : doropomo.value.currentTimer === Counter.SHORTWORK
                  ? "Hora do trabalho longo!"
                  : "Hora do descanso!",
              caption: "Não se esqueça de alongar e beber água!",
              position: "top",
            });
          } else {
            // TODO Notifications
          }

          doropomo.value.currentTimer =
            doropomo.value.currentTimer === Counter.REST
              ? Counter.SHORTWORK
              : doropomo.value.currentTimer === Counter.SHORTWORK
              ? Counter.LONGWORK
              : Counter.REST;
          doropomo.value.clockCount =
            doropomo.value.currentTimer === Counter.REST
              ? doropomo.value.restCount * 60
              : doropomo.value.currentTimer === Counter.SHORTWORK
              ? doropomo.value.shortWorkCount * 60
              : doropomo.value.longWorkCount * 60;
        } else {
          doropomo.value.clockCount = doropomo.value.clockCount - 1;
        }
      }, 1000);
      doropomo.value.isPlaying = true;
    }
  };

  const updateRestCount = async (value) => {
    doropomo.value.restCount = value;
    if (window.__TAURI__) {
      await store.set("restCount", value);
      await store.save();
    } else {
      try {
        LocalStorage.set("restCount", value);
      } catch (e) {
        // data wasn't successfully saved due to
        // a Web Storage API error
      }
    }
    if (
      doropomo.value.currentTimer === Counter.REST &&
      !doropomo.value.isPlaying
    ) {
      doropomo.value.clockCount = value * 60;
    }
  };

  const updateShortWorkCount = async (value) => {
    doropomo.value.shortWorkCount = value;
    if (window.__TAURI__) {
      await store.set("shortWorkCount", value);
      await store.save();
    } else {
      try {
        LocalStorage.set("shortWorkCount", value);
      } catch (e) {
        // data wasn't successfully saved due to
        // a Web Storage API error
      }
    }
    if (
      doropomo.value.currentTimer === Counter.SHORTWORK &&
      !doropomo.value.isPlaying
    ) {
      doropomo.value.clockCount = value * 60;
    }
  };

  const updateLongWorkCount = async (value) => {
    doropomo.value.longWorkCount = value;
    if (window.__TAURI__) {
      await store.set("longWorkCount", value);
      await store.save();
    } else {
      try {
        LocalStorage.set("longWorkCount", value);
      } catch (e) {
        // data wasn't successfully saved due to
        // a Web Storage API error
      }
    }
    if (
      doropomo.value.currentTimer === Counter.LONGWORK &&
      !doropomo.value.isPlaying
    ) {
      doropomo.value.clockCount = value * 60;
    }
  };

  const updateTheme = async (theme) => {
    setCssVar("primary", theme.primary);
    setCssVar("secondary", theme.secondary);
    setCssVar("accent", theme.accent);
    currentTheme.value = theme.name;
    if (window.__TAURI__) {
      await store.set("theme", theme.name);
      await store.save();
    } else {
      LocalStorage.set("theme", theme.name);
    }
  };

  const toggleDarkMode = async (value) => {
    Dark.set(value);

    if (window.__TAURI__) {
      await store.set("darkmode", Dark.isActive);
      await store.save();
    } else {
      LocalStorage.set("darkmode", Dark.isActive);
    }
  };

  const clearTimer = () => {
    clearInterval(loop);
  };

  return {
    doropomo,
    themes,
    currentTheme,
    currentTimer,
    isPlaying,
    textTimer,
    playPause,
    updateRestCount,
    updateShortWorkCount,
    updateLongWorkCount,
    updateTheme,
    toggleDarkMode,
    clearTimer,
  };
});
