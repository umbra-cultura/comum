// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
//use quote::quote;
//use syn;
use tauri::{CustomMenuItem, SystemTrayMenu, SystemTrayMenuItem, SystemTrayEvent, SystemTray, Manager, Window};

#[tauri::command]
async fn close_splashscreen(window: Window) {
  // Close splashscreen
  window.get_window("splashscreen").expect("no window labeled 'splashscreen' found").close().unwrap();
  // Show main window
  window.get_window("main").expect("no window labeled 'main' found").show().unwrap();
}

fn main() {
  let sair = CustomMenuItem::new("sair".to_string(), "Sair");
  let ver = CustomMenuItem::new("ver".to_string(), "Ocultar");
  let tray_menu = SystemTrayMenu::new()
    .add_item(ver)
    .add_native_item(SystemTrayMenuItem::Separator)
    .add_item(sair);

  tauri::Builder::default()
    .setup(|app| {
      let handle = app.handle();
      match app.get_cli_matches() {
        Ok(matches) => match matches.subcommand {
          Some(subcommand) => {
            if subcommand.name == "greet" {
              let args = subcommand.matches.args;
              if let Some(nome) = args.get("nome") {
                println!("Seja bem vinde, {}", nome.value.to_string());
              }
              if let Some(help) = args.get("help") {
                println!("{}", help.value.to_string());
                handle.exit(0);
              }
            }
          }
          None => {
            let args = matches.args;
            if let Some(arg) = args.get("help") {
              println!("{}", arg.value.to_string());
              handle.exit(0);
            }
          }
        },
        Err(_) => {}
      }
      Ok(())
    })
    .system_tray(SystemTray::new().with_menu(tray_menu))
    .on_system_tray_event(|app, event| match event {
      SystemTrayEvent::LeftClick {
        position: _,
        size: _,
        ..
      } => {
        println!("system tray received a left click");
        let window = match app.get_window("main") {
          Some(window) => match window.is_visible().expect("winvis") {
            true => {
              // hide the window instead of closing due to processes not closing memory leak: https://github.com/tauri-apps/wry/issues/590
              window.hide().expect("winhide");
              // window.close().expect("winclose");
              return;
            }
            false => window,
          },
          None => return,
        };
        #[cfg(not(target_os = "macos"))]
        {
            window.show().unwrap();
        }
        window.set_focus().unwrap();
      }
      SystemTrayEvent::RightClick {
        position: _,
        size: _,
        ..
      } => {
        println!("system tray received a right click");
      }
      SystemTrayEvent::DoubleClick {
        position: _,
        size: _,
        ..
      } => {
        println!("system tray received a double click");
      }
      SystemTrayEvent::MenuItemClick { id, .. } => {
        let item_handle = app.tray_handle().get_item(&id);
        match id.as_str() {
          "sair" => {
            std::process::exit(0);
          }
          "ver" => {
            let window = app.get_window("main").unwrap();
            if window.is_visible().unwrap() {
              window.hide().unwrap();
              item_handle.set_title("Mostrar").unwrap();
            }
            else {
              window.show().unwrap();
              window.center().unwrap();
              window.set_focus().unwrap();
              item_handle.set_title("Ocultar").unwrap();
            }
          }
          _ => {}
        }
      }
      _ => {}
    })
    .on_window_event(|event| match event.event() {
      tauri::WindowEvent::CloseRequested { api, .. } => {
        // let item_handle = event.tray_handle().get_item("ver");
        event.window().hide().unwrap();
        // item_handle.set_title("Mostrar").unwrap();
        api.prevent_close();
      }
      _ => {}
    })
    .plugin(tauri_plugin_sql::Builder::default().build())
    .plugin(tauri_plugin_store::Builder::default().build())
    .invoke_handler(tauri::generate_handler![close_splashscreen])
    .run(tauri::generate_context!())
    .expect("error while running tauri application");
}
