SHELL := /bin/bash

PYTHON=python3.10
DJANGO_MANAGE=./manage.py

all:
	@./.comum/create.sh

backend-serve: env-dev migrations
	cd api && poetry run python3 $(DJANGO_MANAGE) runserver

build-dev: env-dev build-python migrations run-django-scripts
	cd app && npm i

build-docs: env-dev
	cd app && npm run build:story

build-python:
	cd api && poetry install

build-dev-frontend: env-dev
	@cd app && npm i && npm run build:ssr

build-prod-frontend: env-prod
	@cd app && npm i && npm run build:ssr

build-dev-frontend-native: env-dev
	@cd app && npm i && npm run tauri build

build-prod-frontend-native: env-prod
	@cd app && npm i && npm run tauri build

configure-vagrant:
	@sudo ./.comum/.production_toolbox/vagrant_etchosts.sh
	@./.comum/.production_toolbox/caddy/vagrant_caddy.sh

clean:
	@rm -rf $(ENV_DIR)
	@rm -rf node_modules app/node_modules
	@rm -rf package-lock.json app/package-lock.json
	@rm -rf app/dist app/src-ssr
	@rm -rf .pytest_cache
	@echo "Environment cleaned."

deploy: env-prod env-sub build-prod-frontend
	echo "Building ${ENVIRONMENT} Environment"
	docker-compose up --build -d

decrypt-dotenv: env-dev
	gpg --quiet --batch --yes --decrypt --passphrase=ENCRYPTION_KEY env.tar.gpg | tar -x

encrypt-dotenv:
	tar -c env/ | gpg --symmetric -c -o env.tar.gpg

env-dev:
	$(eval include env/.env.dev)
	$(eval export $(shell sed 's/=.*//' env/.env.dev))

env-test:
	$(eval include env/.env.test)
	$(eval export $(shell sed 's/=.*//' env/.env.test))

env-prod:
	$(eval include env/.env.prod)
	$(eval export $(shell sed 's/=.*//' env/.env.prod))

env-sub: env-prod
	@envsubst < "docker-compose.prod.yml" > "docker-compose.yml"

flush-the-database-yes-really: env-dev
	cd api && poetry run python3 $(DJANGO_MANAGE) flush

frontend-serve: env-dev
	cd app && npm run dev:ssr

frontend-serve-docs: env-dev build-docs
	cd app && npm run preview:story

frontend-serve-native: env-dev
	cd app && npm run tauri dev -- -- -- greet Comum

generate-icons:
	cd app && icongenie generate -p ./icongenie-icon_profile.json && npm run tauri icon && icongenie verify -p ./icongenie-icon_profile.json

migrations: env-dev
	cd api && poetry run python3 $(DJANGO_MANAGE) makemigrations --noinput
	cd api && poetry run python3 $(DJANGO_MANAGE) migrate --noinput

run-django-scripts: env-dev
	cd api && poetry run python3 $(DJANGO_MANAGE) runscript create_test_users

test: env-dev build-python
	cd api && poetry run coverage run && poetry run coverage report && poetry run coverage xml

test-build: env-dev build-python build-dev-frontend

version: env-dev
	cd app && npm run version