from django.contrib.auth.models import User
from core.models import Profile
from utils import L, LOG
import os


def run():
    """Script to create two test users, one for Guest and one for Admin."""
    LOG.info(f"\nCriando usuários de teste...")
    if not User.objects.filter(username='guest@email.com').exists():
        user=User.objects.create_user(username='guest@email.com', email='guest@email.com', first_name="Guest", password='senhasecreta')
        user.is_superuser=False
        user.is_staff=False
        user.save()
        if not Profile.objects.filter(usuario=user):
            profile = Profile.objects.create(usuario=user, funcao="Visitante do Sistema", is_delegador=False)
            profile.save()
        LOG.info(f"\n{L.SUCCESS}Usuário 'guest' criado com sucesso!{L.ENDC}\n")

    if not User.objects.filter(username='admin').exists():
        user = User.objects.create_user(username='admin', email='admin@email.com', first_name="Administrador",
                                        password=os.environ.get("DJANGO_ADMIN_PASSWORD"))
        user.is_superuser=True
        user.is_staff=True
        user.save()
        if not Profile.objects.filter(usuario=user):
            profile = Profile.objects.create(usuario=user, funcao="Administrador do Sistema", is_delegador=True)
        LOG.info(f"\n{L.SUCCESS}Superusuário 'admin' criado com sucesso!{L.ENDC}\n")
    LOG.info(f"Pronto.")
