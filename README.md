<a id="readme-top"></a>

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=for-the-badge)](https://github.com/prettier/prettier)
![GitLab (self-managed) contributors](https://img.shields.io/gitlab/contributors/umbra-cultura%2Fcomum?style=for-the-badge)
![Gitlab code coverage (self-managed, unit-test-backend)](https://img.shields.io/gitlab/pipeline-coverage/umbra-cultura%2Fcomum?job_name=unit-test-backend&branch=main&style=for-the-badge&label=Backend%20Coverage)
![Gitlab code coverage (self-managed, unit-test-frontend)](https://img.shields.io/gitlab/pipeline-coverage/umbra-cultura%2Fcomum?job_name=unit-test-frontend&branch=main&style=for-the-badge&label=Frontend%20Coverage)
![Gitlab pipeline status (self-managed)](https://img.shields.io/gitlab/pipeline-status/umbra-cultura%2Fcomum?branch=main&style=for-the-badge)
![Mastodon Follow](https://img.shields.io/mastodon/follow/000166251?domain=https%3A%2F%2Fmasto.donte.com.br&style=for-the-badge)
![GitLab all issues](https://img.shields.io/gitlab/issues/all/umbra-cultura%2Fcomum?style=for-the-badge)
![GitLab last commit](https://img.shields.io/gitlab/last-commit/umbra-cultura%2Fcomum?style=for-the-badge)
![GitLab (self-managed)](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg?style=for-the-badge)
![GitLab forks](https://img.shields.io/gitlab/forks/umbra-cultura%2Fcomum?style=for-the-badge)
![GitLab stars](https://img.shields.io/gitlab/stars/umbra-cultura%2Fcomum?style=for-the-badge)
![GitLab tag (self-managed)](https://img.shields.io/gitlab/v/tag/umbra-cultura%2Fcomum?style=for-the-badge)

<br />

<div align="center">
<img src="logo.png"
     alt="logo image" width="230" />
<h1 align="center">Comum Framework</h1>
<p align="center">
<em> Um ambiente de desenvolvimento e produção completo para Django, Vue e Tauri. </em>
<br />
    <a href="https://comum-docs.onrender.com" target="_blank"><strong>Veja a nossa documentação »</strong></a>
    <br />
    <a href="https://comum-backend.onrender.com/schema/" target="_blank"><strong>Veja o schema da nossa API »</strong></a>
    <br />
    <br />
    <a href="https://comum-frontend.onrender.com" target="_blank">Veja a página de exemplo</a>
    ·
    <a href="https://gitlab.com/umbra-cultura/comum/-/issues">Achou um bug? Conte pra nós!</a>
    ·
    <a href="https://gitlab.com/umbra-cultura/comum/-/issues">Teve uma ideia? Conte também!</a>
</p>
</div>
<details>
  <summary>Índice</summary>
  <ol>
    <li>
      <a href="#sobre-o-projeto">Sobre o Projeto</a>
      <ul>
        <li><a href="#feito-com">Feito com</a></li>
      </ul>
    </li>
    <li>
      <a href="#começando">Começando</a>
      <ul>
        <li><a href="#pré-requisitos">Pré-requisitos</a></li>
        <li><a href="#instalação">Instalação</a></li>
      </ul>
    </li>
    <li><a href="#como-usar">Como Usar</a>
      <ul>
        <li><a href="#simulando-o-ambiente-de-produção">Simulando o ambiente de produção</a></li>
        <li>
          <a href="#sobre-o-vscode">Sobre o Vscode</a>
        </li>
        <li>
          <a href="#hospedando-múltiplas-aplicações-web-na-mesma-instância-de-servidor">Hospedando múltiplas aplicações web na mesma instância de servidor</a>
        </li>
        <li>
          <a href="#usuários-para-teste">Usuários para teste</a>
        </li>
        <li>
          <a href="#api">API</a>
        </li>
      </ul>
    </li>
    <li><a href="#roteiro-de-atualizações">Roteiro de Atualizações</a></li>
    <li><a href="#como-contribuir">Como Contribuir</a></li>
    <li><a href="#changelog">Changelog</a></li>
    <li><a href="#licença">Licença</a></li>
    <li><a href="#contato">Contato</a></li>
    <li><a href="#reconhecimentos">Reconhecimentos</a></li>
  </ol>
</details>

## Sobre o Projeto

[![Comum Framework Screen Shot][product-screen]](#)

Comum é um framework para criar aplicações wev desacopladas com Django e Vue.
É essencialmente um modelador rápido de um full-stack. Grande parte do trabalho pesado em configurar tanto o ambiente de desenvolvimento quanto o de produção já foi feito, como a configuração de servidor, ambientes de teste, alocação em containers, SSL/TLS, DNS, testagem, e muito mais.

O conceito por trás do Comum é de remover todas as dependências em cima de si mesmo uma vez configurado.
Comum irá criar tudo o que for necessário, e então irá silenciosamente se remover – deixando uma aplicação Django/Vue limpa, confiável, e pronta para produção.

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

### Feito com

Como Comum toma conta de várias tecnologias subjacentes, certas escolhas já foram feitas. Isso resulta em mais simplicidade e rapidez, mas em menos flexibilidade (relativamente falando). Comum será instalado com as seguintes tecnologias:

- [![Apache Cordova][Cordova]][Cordova-url]
- [![Cypress][Cypress.js]][Cypress-url]
- [![Django][Django]][Django-url]
- [![Django Rest Auth][Django.Rest.Auth]][Django.Rest.Auth-url]
- [![Django Rest Framework][Django.Rest]][Django.Rest-url]
- [![Docker][Docker]][Docker-url]
- [![Gitlab CI/CD][Gitlab]][Gitlab-url]
- [![Histoire][Histoire]][Histoire-url]
- [![Make][Makefile]][Makefile-url]
- [![PostgreSQL][PostgreSQL]][PostgreSQL-url]
- [![PWA][PWA]][PWA-url]
- [![Poetry][Poetry]][Poetry-url]
- [![Python][Python]][Python-url]
- [![Quasar][Quasar.js]][Quasar-url]
- [![SQLite][SQLite-b]][SQLite-url]
- [![Tauri][Tauri.js]][Tauri-url]
- [![Vagrant][Vagrant]][Vagrant-url]
- [![Vite][Vite]][Vite-url]
- [![Vitest][Vitest]][Vitest-url]
- [![Vue][Vue.js]][Vue-url]

Se você gostaria de ver mais escolhas, por favor, considere contribuir para o projeto.

## Começando

Para ter uma cópia local de Comum funcionando, é só seguir as seguintes instruções.

### Pré-requisitos

Comum requer que as seguintes ferramentas já estejam instaladas localmente antes de começar:

- python-virtualenv

- node.js

- npm

### Instalação

Para começar, clone o repositório, substituindo `meu_projeto` abaixo com o nome de projeto desejado.

```bash
git clone https://gitlab.com/umbra-cultura/comum.git meu_projeto
cd meu_projeto
```

Comum é controlado por um `Makefile`. Para começar, apenas execute:

```bash
make
```

Depois de uma rápida instalação, você pode executar:

```bash
make backend-serve
```

e então, em uma nova aba, executar:

```bash
make frontend-serve
```

e sua aplicação estará pronta.

## Como Usar

TODO

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_Para mais exemplos, favor consultar a [Documentação](https://comum-docs.onrender.com)_

_Para mais informações sobre como customizar a configuração do Quasar, veja [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js)_

### Simulando o ambiente de produção

Antes de configurar um servidor real, pode ser extremamente útil simular o ambiente de produção. Para isso, Comum faz uso do Vagrant, uma ferramente que permite que mudanças sejam feitas no seu ambiente de desenvolvimento enquanto espelha o seu código de produção em uma máquina virtual.

Para usar essa funcionalidade, será necessária a instalação do [Vagrant](https://www.vagrantup.com/downloads)
e do [VirtualBox](https://www.virtualbox.org/wiki/Downloads).

O `Vagrantfile` controla a confuguração. Se você não deseja usar essa funcionalidade, você pode remove-lo.

Crie o ambiente com:

```bash
make configure-vagrant
```

A execução do comando pela primeira vez pode levar alguns minutos.

Quando a máquina virtual estiver configurada, você pode acessá-la por meio de:

```bash
vagrant ssh
```

Se a instalação tiver sido completada com sucesso, deverá haver uma instância do CaddyServer executando na máquina virtual.
Isso pode ser verificado usando:

```bash
docker ps
```

Se houver algum problema nesta etapa, faça `cd` para o diretório `/caddy` na máquina virtual e cheque se há algum erro no `Caddyfile`.

Agora, faça o `cd` para o diretório `/djengu` na máquina virtual. Você deverá ver seu projeto lá. Se alguma edição for feita na sua máquina local, Vagrant irá espelhar essas mudanças na máquina virtual, permitindo que você ajuste suas configurações de produção sem a necessidade de fazer mudanças em dois lugares diferentes.

Antes de executar o deploy, é uma boa ideia checar o arquivo `env/.env.prod` para se assegurar de que as configurações estão corretas.

Se estiver tudo certo, execute:

```bash
make deploy
```

Isso irá simular um deploy real. Para ver a aplicação, é só visitar o domínio informado durante a configuração inicial. Se o domínio, ou quaisquer outras configurações que podem afetar o CaddyServer, fore ajustados, lembre-se de re-executar `make configure-vagrant` de sua máquina local (e não pela virtual).

> **NOTA:** A razão pela qual é necessário informar a senha na execução de `make configure-vagrant` é porque Comum precisa de acesso ao arquivo `/etc/hosts`. Esta é a única mudança que Comum faz na sua máquina para além do seu próprio repositório. Quando os testes com o Vagrant estiverem concluídos, é uma boa ideia abrir o arquivo `/etc/hosts` e remover essas linhas. Se você usa o mesmo domínio para testes e para deploy, os endereços podem interferir com seu acesso à applicação real.

Para remover a máquina virtual, execute:

```bash
vagrant destroy
```

do diretório raiz.

### Sobre o Vscode

Se você for um usuário do Visual Studio Code (ou do [VSCodium](https://vscodium.com)), há alguns scripts feitos de antemão para executar o backend no diretório `.vscode/`. Se você não usa o VSCode, o diretório pode ser excluído.

### Hospedando múltiplas aplicações web na mesma instância de servidor

Como Comum é desacoplado por design, é possível hospedar várias aplicações completamente independentes na mesma instância de servidor. Isso é feito usando o reverse-proxy imbutido no CaddyServer.

Isso é muito útil para executar vários websites independentes na mesma instância VPS. Você poderia teoricamente manter todo um portfolio de desenvolvimento composto por sites de baixo tráfego por R$ 25 por mês desta maneira.

Tudo é feito pelo `Caddyfile`, que irá instalar no `/caddy/Caddyfile` na máquina virtual Vagrant. Você pode adicionar aplicações extras e referenciar cada container do Docker.
Ao fazer isso, é importante lembrar de usar números de porta únicos para cada instância, e referenciar esses números no `Caddyfile`. Em produção, você pode colocar o diretório `caddy/` em qualquer lugar, mas você deve adicionar cada aplicação individual no mesmo `Caddyfile`.

### Usuários para teste

Durante a instalação inicial de Comum, uma conta para testes foi incluída. As credenciais são:

```
username: guest
password: senhasecreta
```

Você também pode acessar com o nome de usuário `admin` e a senha de administrador informada durante a instalação.

### API

As applicações de Comum são baseadas em um `Makefile`, que pode ser encontrado no diretório raiz. Aqui está um sumário do que pode ser possível fazer a partir deste arquivo.
Sinta-se a vontade para adicionar suas próprias receitas `Make` ao arquivo.

#### `make`

Executa o script de instalação.

#### `make build-*`

Veja o `Makefile` para as diferentes opções. Executa os diferentes builders para o backend e o frontend.

#### `make configure-vagrant`

Execute para configurar a máquina virtual Vagrant. A execução inicial deste comando irá configurar a máquina virutal baseado no script `.comum/.production_toolbox/server_setup.sh`. Você pode também usar esse script em um servidor real (por ex. quando inicializando um droplet do DigitalOcean).

Se você fizer mudanças em alguns dos nomes de domínio ou em outras configuraçções relacionadas ao servidor no arquivo `.env.prod`, lembre-se de re-executar essa receita.

#### `make clean`

Remove todos os arquivos de build e começa tudo do zero.

#### `make deploy`

Faz o deploy da applicaão no servidor de produção (ou na máquina virtual Vagrant).

#### `make decrypt-dotenv`

Você precisará executar esse comando fora da receita `make`, pois será preciso substituir `foo` pela chave real. **NÂO ADICIONE A CHAVE DE DECODIFICAÇÃO AO MAKEFILE**.

#### `make encrypt-dotenv`

Encripta o diretório `env/`. Executar esta receita irá pedir que você crie uma chave de decodificação. Você pode adicionar esta chave como um secredo do GitLab para permitir o funcionamento do CI/CD.

Esta receita irá criar um arquivo chamado `env.tar.gpg`. Este arquivo pode ser incluído no controle de versão, apenas se assegure de não incluir quaisquer referências à chave de decodificação. Use o comando dentro da receita `make decrypt-dotenv` para decodificar o arquivo no servidor ou no CI/CD
(use o comando, e não a receita em si, já que a chave deve ser informada manualmente).

#### `make frontend-serve`

Executa o ambiente de desenvolvimento para o frontend.

#### `make backend-serve`

Executa o ambiente de desenvolvimento para o backend.

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

## Roteiro de Atualizações

TODO

- [x] Add Changelog
- [x] Add back to top links
- [ ] Add Additional Templates w/ Examples
- [ ] Add "components" document to easily copy & paste sections of the readme
- [ ] Multi-language Support
  - [ ] Chinese
  - [ ] Spanish

See the [open issues](https://gitlab.com/umbra-cultura/comum/-/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

<!-- CONTRIBUTING -->

## Como Contribuir

Contribuições são o que faz a comunidade do código aberto ser um espaço incrível de aprendizado, inspiração e criatividade. Quaisquer contribuições feitas são **enormemente bem-vindas**.

Se você tem uma sugestão de como tornar o projeto melhor, por favor, faça o _fork_ do repositório e crie uma _pull request_. Você também pode abrir uma _issue_ com a _tag_ "Melhoria".

Caso você queira contribuir financeiramente com o desenvolvimento do projeto, considere enviar um PIX usando este QR Code:

[![Pix][pix]](#)

Ou por este código copia-e-cola

```
00020126940014BR.GOV.BCB.PIX0136eb66aca2-23f9-4597-b1fb-a559a82419570232Contribuindo com o projeto Comum5204000053039865802BR5925Fausto Andre de Jesus Fil6009SAO PAULO61080540900062250521rOWLDEIbaaygxAg1fi0lb6304788A
```

Não se esqueça de dar uma estrela ao projeto! Muito obrigado!

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

<!-- LICENSE -->

## Changelog

Veja `CHANGELOG.md` para mais informações.

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

<!-- LICENSE -->

## Licença

[![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

Esta obra tem a [licença Creative Commons Atribuição-NãoComercial-CompartilhaIgual 4.0 Internacional][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

Veja `LICENSE.md` para mais informações.

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

<!-- CONTACT -->

## Contato

Fausto André - [@fslawliet@masto.donte.com.br](https://masto.donte.com.br/@fslawliet) - fslawliet@hotmail.com

Link do projeto: [https://gitlab.com/umbra-cultura/comum](https://gitlab.com/umbra-cultura/comum)

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

<!-- ACKNOWLEDGMENTS -->

## Reconhecimentos

TODO

- [Choose an Open Source License](https://choosealicense.com)
- [Djengu Framework](https://github.com/johnckealy/djengu)
- [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
- [Malven's Flexbox Cheatsheet](https://flexbox.malven.co/)
- [Malven's Grid Cheatsheet](https://grid.malven.co/)
- [Img Shields](https://shields.io)
- [GitHub Pages](https://pages.github.com)
- [Font Awesome](https://fontawesome.com)
- [React Icons](https://react-icons.github.io/react-icons/search)

<p align="right">(<a href="#readme-top">Voltar ao topo</a>)</p>

[cc-by-nc-sa]: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.pt_BR
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
[product-screen]: ./screenshot.png
[pix]: ./pixqr.png
[Cordova]: https://img.shields.io/badge/Cordova-4CC2E4?style=for-the-badge&logo=apachecordova&logoColor=white
[Cordova-url]: https://cordova.apache.org/
[Cypress.js]: https://img.shields.io/badge/Cypress-F0FCF8?style=for-the-badge&logo=cypress&logoColor=black
[Cypress-url]: https://www.cypress.io/
[Django]: https://img.shields.io/badge/Django-0C4B33?style=for-the-badge&logo=django&logoColor=white
[Django-url]: https://www.djangoproject.com
[Django.Rest]: https://img.shields.io/badge/Django%20Rest%20Framework-A30000?style=for-the-badge&logo=django&logoColor=white
[Django.Rest-url]: https://www.django-rest-framework.org
[Django.Rest.Auth]: https://img.shields.io/badge/Django%20Rest%20Auth-2980B9?style=for-the-badge&logo=django&logoColor=white
[Django.Rest.Auth-url]: https://dj-rest-auth.readthedocs.io
[Docker]: https://img.shields.io/badge/docker-2496ED?style=for-the-badge&logo=docker&logoColor=white
[Docker-url]: https://www.docker.com/
[Gitlab]: https://img.shields.io/badge/gitlab%20CI/CD-FC6D26?style=for-the-badge&logo=gitlab&logoColor=white
[Gitlab-url]: https://docs.gitlab.com/ee/ci/
[Histoire]: https://img.shields.io/badge/Histoire-213547?style=for-the-badge&logo=data:image/svg%2bxml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iNTEyIgogICBoZWlnaHQ9IjUxMiIKICAgdmlld0JveD0iMCAwIDUxMiA1MTIiCiAgIHZlcnNpb249IjEuMSIKICAgaWQ9InN2ZzUiCiAgIHNvZGlwb2RpOmRvY25hbWU9ImxvZ28uc3ZnIgogICBpbmtzY2FwZTpleHBvcnQtZmlsZW5hbWU9Ii9ob21lL2Frcnl1bS9QaWN0dXJlcy9Mb2dvcy9oaXN0b2lyZS5wbmciCiAgIGlua3NjYXBlOmV4cG9ydC14ZHBpPSIxMTAuMDI0IgogICBpbmtzY2FwZTpleHBvcnQteWRwaT0iMTEwLjAyNCIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMS4xLjEgKDNiZjVhZTBkMjUsIDIwMjEtMDktMjApIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBpZD0ibmFtZWR2aWV3MTEiCiAgICAgcGFnZWNvbG9yPSIjZmZmZmZmIgogICAgIGJvcmRlcmNvbG9yPSIjNjY2NjY2IgogICAgIGJvcmRlcm9wYWNpdHk9IjEuMCIKICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwLjAiCiAgICAgaW5rc2NhcGU6cGFnZWNoZWNrZXJib2FyZD0iMCIKICAgICBzaG93Z3JpZD0iZmFsc2UiCiAgICAgaW5rc2NhcGU6em9vbT0iMS41MTU2MjUiCiAgICAgaW5rc2NhcGU6Y3g9IjI1NS42NzAxIgogICAgIGlua3NjYXBlOmN5PSIyNTYiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIyNTYwIgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEzNzEiCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjAiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9IjAiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJzdmc1IiAvPgogIDxkZWZzCiAgICAgaWQ9ImRlZnMyIiAvPgogIDxnCiAgICAgaWQ9ImxheWVyMSI+CiAgICA8cmVjdAogICAgICAgc3R5bGU9Im9wYWNpdHk6MC41O2ZpbGw6IzM0ZDM5OTtmaWxsLW9wYWNpdHk6MTtzdHJva2Utd2lkdGg6MS4wMDM3NSIKICAgICAgIGlkPSJyZWN0MTM3MiIKICAgICAgIHdpZHRoPSIzMTQuMzA5MjMiCiAgICAgICBoZWlnaHQ9IjQwNi42MDkwMSIKICAgICAgIHg9Ii0yNi41NjUwNjMiCiAgICAgICB5PSIxMzQuNzUwNzkiCiAgICAgICB0cmFuc2Zvcm09InJvdGF0ZSgtMjMuODIxMjYyKSIKICAgICAgIHJ5PSI4IiAvPgogICAgPHJlY3QKICAgICAgIHN0eWxlPSJmaWxsOiMzNGQzOTk7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlLXdpZHRoOjEuMDAzNzUiCiAgICAgICBpZD0icmVjdDg1MCIKICAgICAgIHdpZHRoPSIzMTQuMzA5MjMiCiAgICAgICBoZWlnaHQ9IjQwNi42MDkwMSIKICAgICAgIHg9Ijc3LjU3MTgzOCIKICAgICAgIHk9IjcyLjgwODcwOCIKICAgICAgIHJ5PSI4IgogICAgICAgdHJhbnNmb3JtPSJyb3RhdGUoLTQuNTc0NDUzNCkiIC8+CiAgPC9nPgogIDxnCiAgICAgaWQ9ImxheWVyMyI+CiAgICA8cGF0aAogICAgICAgaWQ9InBhdGgxNjU3LTMiCiAgICAgICBzdHlsZT0iZGlzcGxheTppbmxpbmU7ZmlsbDojYjRmYWUyO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTojYjRmYWUyO3N0cm9rZS13aWR0aDo4LjM0OTIzO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICBkPSJNIDM1OS4zODk0NywzNTUuOTUxMzQgMzIwLjcyOTM1LDE3Ni41Mjk0MiAyMzguMzQ2MTMsMjM4Ljk0MTE4IFogTSAyNzMuNjQxMjQsMjczLjA2NjA4IDE1Mi41OTc4OCwxNTYuMDU1OTEgMTkxLjI1ODA0LDMzNS40Nzc4NiBaIiAvPgogIDwvZz4KICA8ZwogICAgIGlkPSJsYXllcjIiCiAgICAgc3R5bGU9ImRpc3BsYXk6bm9uZSI+CiAgICA8cGF0aAogICAgICAgaWQ9InBhdGgxNjU3IgogICAgICAgc3R5bGU9ImZpbGw6I2I0ZmFlMjtmaWxsLW9wYWNpdHk6MTtzdHJva2U6I2I0ZmFlMjtzdHJva2Utd2lkdGg6ODtzdHJva2UtbGluZWNhcDpidXR0O3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2Utb3BhY2l0eToxIgogICAgICAgZD0ibSAyOTYsMTAzLjk4MjQyIC0xMzUuNTMxMjUsMTc3Ljk2NjggaCA4OC43MDExNyB6IE0gMjYyLjgzMDA4LDIzMC4wNTA3OCAyMTYsNDA4LjAxNzU4IDM1MS41MzEyNSwyMzAuMDUwNzggWiIKICAgICAgIHRyYW5zZm9ybT0icm90YXRlKC00LjE1NjU1MywyNTYsMjU2LjAwNjkxKSIgLz4KICA8L2c+Cjwvc3ZnPgo=&logoColor=white
[Histoire-url]: https://histoire.dev
[Jwt]: https://img.shields.io/badge/JWT%20Authentication-FFFFFF?style=for-the-badge&logo=jsonwebtokens&logoColor=black
[Jwt-url]: https://jwt.io/
[Makefile]: https://img.shields.io/badge/Make-000000?style=for-the-badge&logo=data%3Aimage%2Fsvg%2Bxml%3Bbase64%2CPHN2ZyB2aWV3Qm94PSIwIDAgMjQgMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI%2BPHBhdGggZD0iTTMuNDAzIDIuNDdhMi4wNiAyLjA2IDAgMCAwLTIuMDYgMi4wNnYxMi4zNjRhMi4wNiAyLjA2IDAgMCAwIDIuMDYgMi4wNmg3Ljk4M2wxLjc1OCAxLjY1NGEuMzEuMzEgMCAwIDAgLjM5OS4wMmwxLjI3LTEuMDI0Yy4zODguMTQ4Ljc2OC4yMjcgMS4xNjYuMjY2bC42ODcgMS40ODlhLjMwNS4zMDUgMCAwIDAgLjM2Mi4xNTdsMi4zMzQtLjcwNGEuMzA4LjMwOCAwIDAgMCAuMjE1LS4zMzVsLS4yNDUtMS42MmMuMTUxLS4xMjUuMjk2LS4yNTUuNDM0LS40MDIuMTM4LS4xNDcuMjYyLS4zMDcuMzg3LS40NzVsMS42Mi4xNTdhLjMxLjMxIDAgMCAwIC4zMjEtLjIzNWwuNTU0LTIuMzc0YS4zLjMgMCAwIDAtLjE4LS4zNTNsLTEuNTI1LS41OTVhNC40MTYgNC40MTYgMCAwIDAtLjM0Mi0xLjE0MWwuOTQyLTEuMzM0YS4zMS4zMSAwIDAgMC0uMDQ2LS4zOTdsLTEuNjEtMS41MDlWNC41MzFhMi4wNiAyLjA2IDAgMCAwLTIuMDYtMi4wNnptMCA0LjEyMWgxNC40MjN2NC4zODNjLS4zMS0uMS0uNjItLjE2OC0uOTQtLjIwNGwtLjY5LTEuNDgzYS4zMDEuMzAxIDAgMCAwLS4zNjQtLjE1NmwtMi4zMzQuNzA0YS4zMDEuMzAxIDAgMCAwLS4yMTUuMzM0bC4yNiAxLjYwNmMtLjE2LjEzNC0uMzEyLjI3LS40NS40MTZhNC43NDMgNC43NDMgMCAwIDAtLjM3NC40NmwtMS42MzEtLjE0MmEuMy4zIDAgMCAwLS4zMi4yMzVsLS41NTYgMi4zNzVhLjMwNi4zMDYgMCAwIDAgLjE4LjM1MmwxLjUyOS41OWMuMDQ2LjI4NS4xMjMuNTYuMjIxLjgzM0gzLjQwM3ptMTIuOTkxIDYuNTk2YTIuMTM2IDIuMTM2IDAgMCAxIDEuNDk3LjU3OCAyLjEzNiAyLjEzNiAwIDAgMSAuMDk3IDMuMDIgMi4xMzYgMi4xMzYgMCAwIDEtMy4wMTguMDk0IDIuMTM2IDIuMTM2IDAgMCAxLS4wOTctMy4wMTggMi4xMzYgMi4xMzYgMCAwIDEgMS41MjEtLjY3NHoiIHN0eWxlPSJmaWxsOiNlZjUzNTA7c3Ryb2tlLXdpZHRoOjEuMDMwMiIvPjwvc3ZnPg%3D%3D
[Makefile-url]: https://www.gnu.org/s/make/manual/make.html
[PostgreSQL]: https://img.shields.io/badge/PostgreSQL-4169E1?style=for-the-badge&logo=postgresql&logoColor=white
[PostgreSQL-url]: https://www.postgresql.org/
[Poetry]: https://img.shields.io/badge/Poetry-60A5FA?style=for-the-badge&logo=poetry&logoColor=white
[Poetry-url]: https://python-poetry.org
[Python]: https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white
[Python-url]: https://www.python.org
[PWA]: https://img.shields.io/badge/Progressive%20Web%20Apps-5A0FC8?style=for-the-badge&logo=pwa&logoColor=white
[PWA-url]: https://web.dev/i18n/pt/progressive-web-apps/
[Quasar.js]: https://img.shields.io/badge/quasar-1976d2?style=for-the-badge&logo=quasar&logoColor=white
[Quasar-url]: https://quasar.dev
[SQLite-b]: https://img.shields.io/badge/SQLite-044A64?style=for-the-badge&logo=sqlite&logoColor=white
[SQLite-url]: https://sqlite.org/
[Tauri.js]: https://img.shields.io/badge/tauri-242526?style=for-the-badge&logo=tauri&logoColor=white
[Tauri-url]: https://tauri.app/
[Vagrant]: https://img.shields.io/badge/Vagrant-1868F2?style=for-the-badge&logo=vagrant&logoColor=white
[Vagrant-url]: https://www.vagrantup.com/
[Vite]: https://img.shields.io/badge/Vite-646CFF?style=for-the-badge&logo=vite&logoColor=white
[Vite-url]: https://vitejs.dev/
[Vitest]: https://img.shields.io/badge/Vitest-6E9F18?style=for-the-badge&logo=vitest&logoColor=white
[Vitest-url]: https://vitest.dev/
[Vue.js]: https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[Vue-url]: https://vuejs.org/
