/* eslint-env serviceworker */

import { clientsClaim, setCacheNameDetails } from "workbox-core";
import {
  precacheAndRoute,
  cleanupOutdatedCaches,
  createHandlerBoundToURL,
} from "workbox-precaching";
import { NetworkFirst } from "workbox-strategies";
import { registerRoute, NavigationRoute } from "workbox-routing";

setCacheNameDetails({ prefix: "comum-offline" });
self.skipWaiting();

// Use with precache injection
precacheAndRoute(self.__WB_MANIFEST, {
  directoryIndex: "/",
});

registerRoute("/", new NetworkFirst(), "GET");
registerRoute(/^http/, new NetworkFirst(), "GET");

self.addEventListener("activate", function (event) {
  event.waitUntil(clientsClaim());
});

cleanupOutdatedCaches();

// Non-SSR fallback to index.html
// Production SSR fallback to offline.html (except for dev)
if (process.env.MODE !== "ssr" || process.env.PROD) {
  registerRoute(
    new NavigationRoute(
      createHandlerBoundToURL(process.env.PWA_FALLBACK_HTML),
      { denylist: [/sw\.js$/, /workbox-(.)*\.js$/] }
    )
  );
}
