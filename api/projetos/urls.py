from django.urls import path
from .views import JobCreateView, JobList, JobUpdateView

urlpatterns = [
    path('create-job/', JobCreateView.as_view(), name='create-job'),
    path('jobs/', JobList.as_view(), name='job-list'),
    path('jobs/<int:pk>/', JobUpdateView.as_view(), name='job-update'),
]