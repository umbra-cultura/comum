from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Job(models.Model):
    TAMANHO_1_9 = 'tamanho_1-9'
    TAMANHO_10_49 = 'tamanho_10-49'
    TAMANHO_50_99 = 'tamanho_50-99'
    TAMANHO_100 = 'tamanho_100'

    CHOICES_TAMANHO = (
        (TAMANHO_1_9, '1-9'),
        (TAMANHO_10_49, '10-49'),
        (TAMANHO_50_99, '50-99'),
        (TAMANHO_100, '100+')
    )

    ATIVO = 'ativo'
    OCUPADO = 'ocupado'
    ARQUIVADO = 'arquivado'

    CHOICES_STATUS = (
        (ATIVO, 'Ativo'),
        (OCUPADO, 'Ocupado'),
        (ARQUIVADO, 'Arquivado')
    )

    titulo = models.CharField(max_length=255)
    descricao_curta = models.TextField()
    descricao_longa = models.TextField(blank=True, null=True)
    nome_companhia = models.CharField(max_length=255, default='')
    endereco_companhia = models.CharField(max_length=255, null=True, blank=True)
    cep_companhia = models.CharField(max_length=255, null=True, blank=True)
    cidade_companhia = models.CharField(max_length=255, null=True, blank=True)
    pais_companhia = models.CharField(max_length=255, null=True, blank=True)
    tamanho_companhia = models.CharField(max_length=20, choices=CHOICES_TAMANHO, default=TAMANHO_1_9)
    criado_por = models.ForeignKey(User, related_name='jobs', on_delete=models.CASCADE)
    criado_em = models.DateTimeField(auto_now_add=True)
    modificado_em = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=20, choices=CHOICES_STATUS, default=ATIVO)

    def __str__(self):
        return self.titulo
