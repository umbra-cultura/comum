from django.http import JsonResponse
from django.middleware.csrf import get_token
from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from .serializers import UserSerializer
from .models import Profile

@ensure_csrf_cookie
def set_csrf_token(request):
    """
    This will be `/api/set-csrf-cookie/` on `urls.py`
    """
    return JsonResponse({"token": get_token(request), "active": True})

# Create your views here.
class ProfileDetail(generics.GenericAPIView):
    """An extension of the User model for custom logic about the user."""
    permission_classes = [ IsAuthenticated ]
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        """Get the User profile information, if it exists."""
        user = request.user
        try:
            profile_serializer = UserSerializer(user)
            return Response(profile_serializer.data)
        except:
            return Response({ 'message': 'Nothing here.'})


    def post(self, request, format=None):
        """Get or create a user Profile and modify it."""
        try:
            profile = Profile.objects.get(user=request.user)
        except:
            profile = Profile.objects.create(user=request.user)

        # Add your logic for modifying the user Profile here

        return Response(ProfileSerializer(profile).data)
