import { api } from "boot/axios";
import { defineStore } from "pinia";
import { ref, computed } from "vue";
import { useRouter } from "vue-router";
import { Cookies, Notify, SessionStorage } from "quasar";
import { getClient, Body, ResponseType } from "@tauri-apps/api/http";
import { Database } from "../utils/tauri-sql";

export const useAuthStore = defineStore("authStore", () => {
  const router = useRouter();

  const user = ref(null),
    returnUrl = ref(null);

  const IsAuthenticated = computed(() => !!user.value),
    getUser = computed(() => user.value),
    getCsrfToken = computed(() => csrfToken.value);

  const login = async (username, password) =>
    new Promise(async (resolve, reject) => {
      let token = null;
      if (window.__TAURI__ && !Cookies.has("csrftoken")) {
        const db = await Database.load("sqlite:test.db");
        const result = await db.select("SELECT * FROM tokens");
        if (result && result.length > 0) {
          token = result[result.length - 1].token;
        }
      }

      try {
        let resp = null;

        if (window.__TAURI__) {
          const client = await getClient();
          resp = await client.post(
            api.getUri() + "/login/",
            Body.json({
              username,
              password,
            }),
            {
              headers: {
                "X-CSRFToken": token || Cookies.get("csrftoken"),
                "Content-Type": "application/json",
              },
              withCredentials: true,
              xsrfCookieName: "csrftoken",
              xsrfHeaderName: "X-CSRFToken",
            }
          );
        } else {
          resp = await api.post(
            "login/",
            { username, password },
            {
              headers: {
                "X-CSRFToken": token || Cookies.get("csrftoken"),
                "Content-Type": "application/json",
              },
              withCredentials: true,
              xsrfCookieName: "csrftoken",
              xsrfHeaderName: "X-CSRFToken",
            }
          );
        }
        if (
          window.__TAURI__ &&
          !Cookies.has("csrftoken") &&
          !Cookies.has("sessionId")
        ) {
          const db = await Database.load("sqlite:test.db");
          await db.execute(
            "UPDATE tokens SET sessionID = $1 WHERE token = $2",
            [resp.data.key, token]
          );
        } else {
          SessionStorage.set("auth", resp.data.key);
        }
        Notify.create({
          type: "positive",
          message: "Login realizado com sucesso!",
          position: "top",
        });
        resolve(true);
      } catch (error) {
        Notify.create({
          type: "negative",
          message: "Erro no Login",
          caption: error.message,
          position: "top",
        });
        reject(error);
      }
    });
  const logout = async () => {
    let token = null;
    let auth = null;
    if (window.__TAURI__ && !Cookies.has("csrftoken")) {
      const db = await Database.load("sqlite:test.db");
      const result = await db.select("SELECT * FROM tokens");
      if (result && result.length > 0) {
        token = result[result.length - 1].token;
        auth = result[result.length - 1].sessionID;
      }
    }
    try {
      if (window.__TAURI__) {
        const client = await getClient();
        await client.post(api.getUri() + "/login/", Body.json({}), {
          headers: {
            Authorization: `Token ${auth || SessionStorage.getItem("auth")}`,
            "X-CSRFToken": token || Cookies.get("csrftoken"),
            "Content-Type": "application/json",
          },
          xsrfCookieName: "csrftoken",
          xsrfHeaderName: "X-CSRFToken",
        });
      } else {
        await api.post(
          "logout/",
          {},
          {
            headers: {
              Authorization: `Token ${auth || SessionStorage.getItem("auth")}`,
              "X-CSRFToken": token || Cookies.get("csrftoken"),
              "Content-Type": "application/json",
            },
            xsrfCookieName: "csrftoken",
            xsrfHeaderName: "X-CSRFToken",
          }
        );
      }
      Cookies.remove("sessionid");
      SessionStorage.clear();
      if (window.__TAURI__ && !Cookies.has("csrftoken")) {
        const db = await Database.load("sqlite:test.db");
        db.execute("DELETE * from tokens");
      }
      user.value = null;
      Notify.create({
        type: "positive",
        message: "Logout realizado com sucesso!",
        position: "top",
      });
    } catch (error) {
      console.log(error.message);
    }
  };

  const fetchProfile = async () => {
    let token = null;
    let auth = null;
    if (window.__TAURI__ && !Cookies.has("csrftoken")) {
      const db = await Database.load("sqlite:test.db");
      const result = await db.select("SELECT * FROM tokens");
      if (result && result.length > 0) {
        token = result[result.length - 1].token;
        auth = result[result.length - 1].sessionID;
      }
    }

    try {
      let resp = null;

      if (window.__TAURI__) {
        const client = await getClient();
        resp = await client.get(api.getUri() + "/perfil/", {
          headers: {
            Authorization: `Token ${auth || SessionStorage.getItem("auth")}`,
            "X-CSRFToken": token || Cookies.get("csrftoken"),
            "Content-Type": "application/json",
          },
          responseType: ResponseType.JSON,
          timeout: 30,
          xsrfCookieName: "csrftoken",
          xsrfHeaderName: "X-CSRFToken",
        });
      } else {
        resp = await api.get("perfil/", {
          headers: {
            Authorization: `Token ${auth || SessionStorage.getItem("auth")}`,
            "X-CSRFToken": token || Cookies.get("csrftoken"),
            "Content-Type": "application/json",
          },
          xsrfCookieName: "csrftoken",
          xsrfHeaderName: "X-CSRFToken",
        });
      }
      user.value = resp.data;
      return resp.status;
    } catch (error) {
      Notify.create({
        type: "negative",
        message: "Usuário não logado!",
        caption: error.message,
        position: "top",
      });
    }
    return 401;
  };

  const getCsrf = async () => {
    try {
      let resp = null;
      if (window.__TAURI__) {
        const client = await getClient();
        resp = await client.get(api.getUri() + "/get_token/");
      } else {
        resp = api.get("get_token/");
      }
      if (resp.data.active) {
        if (window.__TAURI__ && !Cookies.has("csrftoken")) {
          const db = await Database.load("sqlite:test.db");
          const result = await db.execute(
            "INSERT into tokens (token, active) VALUES ($1, $2)",
            [resp.data.token, resp.data.active]
          );
          console.log(result);
        }
      } else {
        Notify.create({
          type: "negative",
          message: "Erro ao gerar token CSRF",
          position: "top",
        });
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  const setReturnURL = (new_url) => (returnUrl.value = new_url);

  return {
    user,
    returnUrl,
    IsAuthenticated,
    getUser,
    getCsrfToken,
    login,
    logout,
    fetchProfile,
    getCsrf,
    setReturnURL,
  };
});
