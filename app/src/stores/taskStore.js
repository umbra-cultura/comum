import { api } from "boot/axios";
import { defineStore } from "pinia";
import { ref, computed } from "vue";
import { Cookies, Notify, SessionStorage } from "quasar";

export const useTaskStore = defineStore("taskStore", () => {
  const taskList = ref([]);
  const jobList = ref([]);

  const getTaskList = computed(() => taskList.value);
  const getJobList = computed(() => jobList.value);
  const getActiveJobList = computed(() =>
    jobList.value.filter((job) => job.status === "Ativo")
  );
  const getOccupiedJobList = computed(() =>
    jobList.value.filter((job) => job.status === "Ocupado")
  );
  const getArchivedJobList = computed(() =>
    jobList.value.filter((job) => job.status === "Arquivado")
  );

  const pullJobList = () => {
    let auth = null;
    api
      .get("v1/projetos/jobs/", {
        headers: {
          Authorization: `Token ${auth || SessionStorage.getItem("auth")}`,
          "X-CSRFToken": Cookies.get("csrftoken"),
          "Content-Type": "application/json",
        },
        withCredentials: true,
        xsrfCookieName: "csrftoken",
        xsrfHeaderName: "X-CSRFToken",
      })
      .then((resp) => {
        if (resp.status === 200) {
          jobList.value = resp.data;
          Notify.create({
            type: "positive",
            message: "Projetos requisitados com sucesso!",
            caption: `Total de tarefas: ${jobList.value.length}`,
            position: "top",
          });
          console.log(jobList.value);
        } else {
          Notify.create({
            type: "negative",
            message: "Não foi possível requisitar os projetos!",
            position: "top",
          });
        }
      });
  };

  const pullTaskList = () => {
    let auth = null;
    api
      .get("api/tasks/", {
        headers: {
          Authorization: `Token ${auth || SessionStorage.getItem("auth")}`,
          "X-CSRFToken": Cookies.get("csrftoken"),
          "Content-Type": "application/json",
        },
        withCredentials: true,
        xsrfCookieName: "csrftoken",
        xsrfHeaderName: "X-CSRFToken",
      })
      .then((resp) => {
        if (resp.status === 200) {
          taskList.value = resp.data;
          Notify.create({
            type: "positive",
            message: "Tarefas requisitadas com sucesso!",
            caption: `Total de tarefas: ${taskList.value.length}`,
            position: "top",
          });
        } else {
          Notify.create({
            type: "negative",
            message: "Não foi possível requisitar as tarefas!",
            position: "top",
          });
        }
      });
  };
  const addJob = async (job) => {
    let auth = null;
    console.log(job);
    api
      .post("v1/projetos/create-job/", job, {
        headers: {
          Authorization: `Token ${auth || SessionStorage.getItem("auth")}`,
          "X-CSRFToken": Cookies.get("csrftoken"),
          "Content-Type": "application/json",
        },
        withCredentials: true,
        xsrfCookieName: "csrftoken",
        xsrfHeaderName: "X-CSRFToken",
      })
      .then((resp) => {
        if (resp.status === 201) {
          Notify.create({
            type: "positive",
            message: "Job adicionado com sucesso!",
            caption: `Total de jobs: ${jobList.value.length + 1}`,
            position: "top",
          });
        } else if (resp.status === 401) {
          Notify.create({
            type: "negative",
            message: "Não foi possível adicionar o job!",
            caption: resp.data.detail,
            position: "top",
          });
        }
      })
      .finally(() => pullJobList());
  };

  const addTask = (descricao, status) => {
    let auth = null;
    console.log(descricao, status);
    api
      .post(
        "api/tasks/",
        {
          descricao,
          status,
        },
        {
          headers: {
            Authorization: `Token ${auth || SessionStorage.getItem("auth")}`,
            "X-CSRFToken": Cookies.get("csrftoken"),
            "Content-Type": "application/json",
          },
          withCredentials: true,
          xsrfCookieName: "csrftoken",
          xsrfHeaderName: "X-CSRFToken",
        }
      )
      .then((resp) => {
        if (resp.status === 201) {
          Notify.create({
            type: "positive",
            message: "Tarefa adicionada com sucesso!",
            caption: `Total de tarefas: ${taskList.value.length + 1}`,
            position: "top",
          });
        } else {
          Notify.create({
            type: "negative",
            message: "Não foi possível adicionar a tarefa!",
            position: "top",
          });
        }
      })
      .finally(() => pullTaskList());
  };

  return {
    taskList,
    jobList,
    getTaskList,
    getJobList,
    getActiveJobList,
    getOccupiedJobList,
    getArchivedJobList,
    pullJobList,
    pullTaskList,
    addJob,
    addTask,
  };
});
