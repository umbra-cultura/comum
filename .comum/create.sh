#!/usr/bin/env bash
set -e

NC='\033[0m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
RED='\033[0;31m'
BLUE='\033[0;94m'

# Mensagem introdutória

echo -e "${RED}"
echo " ☭☭☭☭☭☭\                                                  "
echo "☭☭  __☭☭\                                                 "
echo "☭☭ /  \__| ☭☭☭☭☭☭\  ☭☭☭☭☭☭\☭☭☭☭\  ☭☭\   ☭☭\ ☭☭☭☭☭☭\☭☭☭☭\  "
echo "☭☭ |      ☭☭  __☭☭\ ☭☭  _☭☭  _☭☭\ ☭☭ |  ☭☭ |☭☭  _☭☭  _☭☭\ "
echo "☭☭ |      ☭☭ /  ☭☭ |☭☭ / ☭☭ / ☭☭ |☭☭ |  ☭☭ |☭☭ / ☭☭ / ☭☭ |"
echo "☭☭ |  ☭☭\ ☭☭ |  ☭☭ |☭☭ | ☭☭ | ☭☭ |☭☭ |  ☭☭ |☭☭ | ☭☭ | ☭☭ |"
echo "\☭☭☭☭☭☭  |\☭☭☭☭☭☭  |☭☭ | ☭☭ | ☭☭ |\☭☭☭☭☭☭  |☭☭ | ☭☭ | ☭☭ |"
echo " \______/  \______/ \__| \__| \__| \______/ \__| \__| \__|"
                                                          
echo
echo "☭☭☭ Bem vindo ao Comum! ☭☭☭"
echo "Para começar, só serão necessárias"
echo "algumas opções de configuração básicas."
echo -e "${NC}"

# Verificação de dependências

if ! which make &> /dev/null
then
    echo -e "${RED}----- Make não foi encontrado! ------"
    echo -e "${NC}Comum depende do comando make."
    echo -e "Por favor, instale-o antes de continuar."
    exit
fi
if ! which python3.10 &> /dev/null
then
    echo -e "${RED}----- python3.10 não foi encontrado! ------"
    echo -e "${NC}Comum depende do Python 3.10."
    echo -e "Por favor, instale-o antes de continuar."
    exit
fi
if ! which poetry &> /dev/null
then
    echo -e "${RED}----- Poetry não foi encontrado! ------"
    echo -e "${NC}Comum depende do comando poetry."
    echo -e "Por favor, instale-o antes de continuar."
    exit
fi

# Inputs do usuário
echo
read -p "Escolha um nome de usuário para a base de dados: " SQL_USER
read -p "Defina um nome para a base de dados: " SQL_DATABASE
read -sp "Defina uma senha para a base de dados: " SQL_PASSWORD
echo
echo -e  "Escolha uma URL para o frontend (Ex: ${BLUE}exemplo.com${NC})\c:"
read -p ": " FRONTEND_URL
echo -e  "Escolha uma URL para o backend (${ORANGE}Deve possuir o mesmo domínio"
echo -e  "primário do URL do frontend, ex: ${BLUE}api.exemplo.com${NC})\c:"
read -p ": " BACKEND_URL
echo -e  "Escolha uma URL para a documentação (${ORANGE}Deve possuir o mesmo domínio"
echo -e  "primário do URL do frontend, ex: ${BLUE}docs.exemplo.com${NC})\c:"
read -p ": " DOCUMENTATION_URL
echo
read -sp "Defina uma senha de administrador do backend: " DJANGO_ADMIN_PASSWORD
echo
echo -e "${GREEN}Muito bem! Caso seja necessário modificar estas opções"
echo "posteriormente, elas podem ser encontradas no diretório env/."
echo
echo -e "${ORANGE}Tudo pronto para configurar Comum. Só vai"
echo "levar uns instantes."
echo -e "${BLUE}"

export SQL_USER
export SQL_DATABASE
export SQL_PASSWORD
export DJANGO_ADMIN_PASSWORD
export FRONTEND_URL
export BACKEND_URL
export DOCUMENTATION_URL
export SECRET_KEY=`< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c45`
export DJENGU_ROOT="${PWD}"

while true; do
    read -p "Tudo pronto? (S/n) " yn
    case $yn in [Ss]* )
        envsubst < Vagrantfile_template > Vagrantfile;
        # rm Vagrantfile_template
        envsubst < ".comum/env_templates/.env.template.db" > "./env/.env.db";
        envsubst < ".comum/env_templates/.env.template.dev" > "./env/.env.dev";
        envsubst < ".comum/env_templates/.env.template.prod" > "./env/.env.prod";
        make build-dev
        break;;


        [Nn]* ) exit;;
        * ) echo "Por favor digite sim ou não.";;
    esac
done

# Desvincular história de Comum do novo projeto
# rm -rf .git/
# git init

echo
echo -e "--------------------------------------------------------"
echo
echo -e "${GREEN}\n🚀 Tudo pronto!"
echo
echo "Comum foi instalado com sucesso. Para entender a estrutura"
echo "do projeto, por favor leia a documentação:"
echo
echo " (TODO) https://github.com/johnckealy/djengu/blob/main/README.md"
echo
echo -e "${NC}AVISO: O diretório ${ORANGE}env/${NC} não está sob controle"
echo -e "de versão, já que adiciona-lo iria expor senhas e outros segredos."
echo -e "Ao invés disso, use ${ORANGE}make encrypt-dotenv${NC}."
echo
echo "--------------------------------------------------------"
echo

# Etapas finais de configuração
while true; do
    echo "Os arquivos de Comum podem agora ser removidas com segurança,"
    echo "mas devem ser mantidas caso a máquina virtual vagrant seja"
    echo "usada para testar a produção."
    echo -e "${BLUE}"
    read -p "Remover os arquivos agora? (S/n) " yn
    case $yn in [Ss]* )
        rm -rf ./.djengu/
        echo "Arquivos removidos."
        break;;
        [Nn]* ) exit;;
        * ) echo "Por favor digite sim ou não.";;
    esac
done