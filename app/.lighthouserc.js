module.exports = {
  ci: {
    collect: {
      url: ["http://localhost:3000/"],
      startServerCommand: "cd ./dist/ssr && npm start",
      startServerReadyPattern: "Server listening at port",
      settings: { chromeFlags: "--no-sandbox" },
      numberOfRuns: 2,
    },
    assert: {
      // assert options here
      preset: "lighthouse:recommended",
    },
    upload: {
      target: "temporary-public-storage",
    },
  },
};
