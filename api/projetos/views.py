from rest_framework.views import APIView
from .models import Job
from .serializers import JobSerializer
from django.http import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework import generics

# Create your views here.
class JobCreateView(APIView):
    """create view"""
    def post(self, request):
        data = JSONParser().parse(request)
        serializer = JobSerializer(data=data)
        if serializer.is_valid():
            serializer.save(criado_por=request.user)
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class JobList(generics.ListAPIView):
    """get view"""
    queryset = Job.objects.all()
    serializer_class = JobSerializer

class JobUpdateView(generics.UpdateAPIView):
    """update view"""
    queryset = Job.objects.all()
    serializer_class = JobSerializer