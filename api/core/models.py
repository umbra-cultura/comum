from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

def user_directory_path(instance, filename): 
  
    # file will be uploaded to MEDIA_ROOT / user_<id>/<filename> 
    return 'media/user_{0}/{1}'.format(instance.usuario.id, filename) 

# Create your models here.
class Profile(models.Model):
    """ A one to one model for extra user fields"""
    usuario = models.OneToOneField(User, related_name='perfil', on_delete=models.CASCADE, null=True)
    funcao = models.CharField(max_length=120)
    avatar = models.ImageField(upload_to=user_directory_path, null=True, blank=True)
    is_delegador = models.BooleanField(default=False)

#class Messages(models.Model):
#   aplicacao = models.ForeignKey(Application, related_name='messages', on_delete=models.CASCADE)
#    conteudo = models.TextField()
#    criado_por = models.ForeignKey(User, related_name='messages', on_delete=models.CASCADE)
#    criado_em = models.DateTimeField(auto_now_add=True)

#    class Meta:
#        ordering = ['criado_em']

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
