import pytest
import uuid
import datetime
from scripts import create_test_users
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from django.urls import reverse
from django.utils import timezone
from rest_framework.authtoken.models import Token
from projetos.models import Job
from django.contrib.auth.models import User

client = APIClient()

@pytest.fixture
def test_password():
   return 'strong-test-pass'

@pytest.fixture
def create_user(db, django_user_model, test_password):
   def make_user(**kwargs):
       kwargs['password'] = test_password
       if 'username' not in kwargs:
           kwargs['username'] = str(uuid.uuid4())
       return django_user_model.objects.create_user(**kwargs)
   return make_user

@pytest.mark.django_db
def test_schema_view():
   url = reverse('api_schema')
   response = client.get(url)
   assert response.status_code == 200

@pytest.mark.django_db
def test_job_list(create_user):
   user = create_user(username='someone')
   token, _ = Token.objects.get_or_create(user=user)
   url = reverse('job-list')
   client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
   response = client.get(url)
   assert response.status_code == 200


@pytest.mark.django_db
def test_superuser_detail(create_user):
   admin_user = create_user(
       username='custom-admin-name',
       is_staff=True, is_superuser=True
   )
   token, _ = Token.objects.get_or_create(user=admin_user)
   url = reverse('job-list')
   client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
   response = client.get(url)
   assert response.status_code == 200