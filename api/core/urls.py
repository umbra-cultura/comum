from django.urls import path, include
from .views import ProfileDetail, set_csrf_token

urlpatterns = [
    path('perfil/', ProfileDetail.as_view(), name='perfil'),
    path('', include('dj_rest_auth.urls')),
    path('get_token/', set_csrf_token, name='get_token'),
]