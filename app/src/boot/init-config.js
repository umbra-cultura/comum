import { Dark, LocalStorage, setCssVar } from "quasar";
import { boot } from "quasar/wrappers";
import { Store } from "../utils/tauri-store";
import { Database } from "../utils/tauri-sql";
import { useAuthStore } from "stores/authStore";
import { useConfigStore } from "stores/configStore";

export default boot(async ({ store }) => {
  const authStore = useAuthStore(store);
  const configStore = useConfigStore(store);

  let tauriTheme = null;
  let tauriDarkMode = null;
  let systemDarkMode =
    window.matchMedia &&
    window.matchMedia("(prefers-color-scheme: dark)").matches;
  let db = null;
  if (window.__TAURI__) {
    const tauriConfigStore = new Store(".comum.settings.json");
    tauriTheme = await tauriConfigStore.get("theme");
    tauriDarkMode = await tauriConfigStore.get("darkmode");
    db = await Database.load("sqlite:test.db");
    await db.execute('DROP TABLE IF EXISTS "tokens"');
    await db.execute(
      'CREATE TABLE IF NOT EXISTS "tokens" ("token"	TEXT NOT NULL UNIQUE, "sessionID"	TEXT, "active" INTEGER NOT NULL, PRIMARY KEY("token"));'
    );
  }

  await authStore.fetchProfile();

  let currentTheme = "Theme 1";

  if (LocalStorage.getItem("theme"))
    currentTheme = LocalStorage.getItem("theme");
  else if (tauriTheme) currentTheme = tauriTheme;

  Dark.set(
    tauriDarkMode || LocalStorage.getItem("darkmode") || systemDarkMode || false
  );

  for (let theme of configStore.themes) {
    if (theme.name === currentTheme) {
      setCssVar("primary", theme.primary);
      setCssVar("secondary", theme.secondary);
      setCssVar("accent", theme.accent);
    }
  }
});
